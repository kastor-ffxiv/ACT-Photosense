# ACT-Photosense

This is a small ACT overlay that aims to reduce the seizure and headache inducing effects of some intense lighting and screen effects in FFXIV. 

It works by looking for the casts of certain boss spells and applying a timed, semi-transparent black overlay to the screen to dampen the light.

***PLEASE USE WITH CAUTION*** - While this plugin is made with the best intentions, I cannot gaurentee it will work for everyone and I cannot gaurentee it will always function.
Use your best judgement and don't get hurt for a video game.

## Setup/Installation

Please follow this video until I create more detailed instructions: https://www.youtube.com/watch?v=rGhH0zTZG_M

The overlay URL is `https://kastor-ffxiv.github.io/ACT-Photosense/`

## List Of Effects By Encounter

This plugin currently works for the following mechanics, listed by encounter.

### E12S

Lion Breath Cones - Lasts for 30 seconds after the Lion Sculptures spawn.
